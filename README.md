## Comment utiliser le projet

* Recupérer le projet en local:     
`git clone git@gitlab.com:exos-api-video/server.git`

* Lancer docker-compose:    
`docker-compose up`

* Récupérer les dépendances:    
`docker-compose exec server composer install`

* L'api est accessible sur `http://localhost:8080`

* Lancer les tests fonctionnels:    
`docker-compose exec server php bin/phpunit`