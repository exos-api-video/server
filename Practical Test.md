﻿Practical Test
==============


# 1. REST Server

Create a REST server.
Please implement the API as described below.

## API

### Request sample

```
POST /videos

name: xxx
duration: 30
```

### Response sample

```
201 Created
```
```json
{
  "id": "<generated_identifier>",
  "name": "xxx",
  "duration": 30
}
```

Note : `<generated_identifier>` must be a unique identifier.

### Request sample

```
GET /videos/<identifier>
```

### Response sample

```
200 OK
```
```json
{
  "id": "<identifier>",
  "name": "Name of the video",
  "duration": 30
}
```

### Request sample

```
GET /videos
```

### Response sample

```
200 OK
```
```json
{
    "videos": [
        {
          "id": "<identifier>",
          "name": "xxx",
          "duration": 30
        },
        ... etc
    ]
}
```

### Request sample

```
DELETE /videos/<identifier>
```

### Response sample

```
204 No content
```


## Runtime

The server must be reachable from an URL like this one:
[http://localhost/](http://localhost/)

Note : the port does not matter.


### Curl samples

```bash
curl -X GET http://localhost/videos
curl -X POST --data "name=xxx&duration=30" http://localhost/videos
curl -X GET http://localhost/videos/<generated_identifier>
curl -X DELETE http://localhost/videos/<generated_identifier>
```


# 2. REST Client

Code a PHP client that can implement your REST server API.
The client must implement the following requirements.

## API

```php
<?php

interface VideoClient {
    /**
     * Create a video
     * 
     * @param string The video name
     * @param int The video duration
     * @return array The array representation of the JSON video
     */
    public function createVideo($name, $duration);
    
    /**
     * Get all videos
     * 
     * @return array The array representation of the JSON video list
     */
    public function getVideos();

    /**
     * Get a video
     * 
     * @return array The array representation of the JSON video 
     */
    public function getVideo($id);
}
```

## Runtime

Write 3 PHP scripts that use the Client.

### Command sample

```bash
php create-video.php toto 60
```

### Output sample

```json
{
  "id": "<generated_identifier>",
  "name": "toto",
  "duration": 60
}
```

### Command sample

```bash
php show-video.php <identifier>
```

### Output sample

```json
{
  "id": "<identifier>",
  "name": "toto",
  "duration": 60
}
```

### Command sample

```bash
php list-videos.php
```

### Output sample
```json
{
    "videos": [
        {
          "id": "<identifier>",
          "name": "toto",
          "duration": 60
        },
        etc...
    ]
}
```