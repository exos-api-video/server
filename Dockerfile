FROM php:7.3-fpm-alpine

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

WORKDIR /app

RUN mkdir -p /tmp/datastore \
    && chown -R www-data /tmp/datastore