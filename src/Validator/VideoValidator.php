<?php
namespace App\Validator;

use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;

class VideoValidator
{

    const ERROR_NOT_FOUND = "Ressource not found";
    const ERROR_BAD_UUID = "Invalid UUID";

    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function isValidUuid($uuid)
    {
        $uuidConstraint = new Assert\Uuid();
        $uuidConstraint->message = self::ERROR_BAD_UUID;
 
        $errors = $this->validator->validate(
            $uuid,
            $uuidConstraint
        );

        if (0 !== count($errors)) {
            return $errors;
        }

        return true;

    }

    public function isValidPost($post)
    {
        $constraint = new Assert\Collection([
            'name' => [
                new Assert\Type('string'),
                new Assert\NotBlank(),
            ],
            'duration' => [
                new Assert\Type('numeric'),
                new Assert\NotBlank(),
            ]
        ]);
        
        $errors = $this->validator->validate($post, $constraint);

        if (0 !== count($errors)) {
            return $errors;
        }

        return true;

    }
}