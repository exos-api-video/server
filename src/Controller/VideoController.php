<?php
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Service\VideoService;
use App\Validator\VideoValidator;


class VideoController extends AbstractController
{
    /**
     * @Route("/videos/{uuid}", methods={"GET"})
     */
    public function getOne(
        $uuid, 
        VideoService $videoService,
        VideoValidator $videoValidator
    )
    {
        $errors = $videoValidator->isValidUuid($uuid);
        if ($errors !== true) {
            return $this->parametersError($errors);
        }

        $video = $videoService->findOne($uuid);

        if(false === $video){
            return $this->notFoundError();
        }

        return $this->json($video);
    }

    /**
     * @Route("/videos", methods={"GET"})
     */
    public function getAll(VideoService $videoService)
    {

        $videos = $videoService->findAll();
        if(false === $videos){
            return $this->notFoundError();
        }
        return $this->json($videos);
       
    }

    /**
     * @Route("/videos", methods={"POST"})
     */
    public function create(
        Request $request, 
        VideoService $videoService, 
        VideoValidator $videoValidator
    )
    {
        $post = $request->request->all();

        $errors = $videoValidator->isValidPost($post);
        if ($errors !== true) {
            return $this->parametersError($errors);
        }

        $newVideo = $videoService->insert($post);
        if(false === $newVideo) {
            return $this->notFoundError();
        }

        return $this->json($newVideo, Response::HTTP_CREATED);
    }

    /**
     * @Route("/videos/{uuid}", methods={"DELETE"})
     */
    public function delete(
        $uuid, 
        VideoService $videoService, 
        VideoValidator $videoValidator
    )
    {
        $errors = $videoValidator->isValidUuid($uuid);
        if ($errors !== true) {
            return $this->parametersError($errors);
        }

        $delete = $videoService->delete($uuid);
        if(false === $delete) {
            return $this->notFoundError();
        }

        return $this->json([], Response::HTTP_NO_CONTENT);
    }

    private function parametersError($errors)
    {
        $errorsArray = [
            'errors' => []
        ];
        foreach($errors as $error){
            $errorsArray['errors'][] = [
                'parameter' =>  $error->getPropertyPath(),
                'message' => $error->getMessage()
            ];
        }
        return $this->json($errorsArray, Response::HTTP_BAD_REQUEST);
    }

    private function notFoundError()
    {
        return $this->json([
                'errors' => [
                    'message' => VideoValidator::ERROR_NOT_FOUND
                ]
            ], 
            Response::HTTP_BAD_REQUEST
        );
    }
}