<?php
namespace App\Service;

use Ramsey\Uuid\Uuid;
use Symfony\Component\Finder\Finder;

class VideoService
{
    const DATASTORE_PATH = '/tmp/datastore';

    public function findOne($uuid)
    {
        if(!file_exists($this->getFileName($uuid))){
            return false;
        }
   
        return json_decode(
            file_get_contents(
                $this->getFileName($uuid)
            )
        );
    
    }

    public function findAll()
    {
        $finder = new Finder();
        $finder->files()->in(self::DATASTORE_PATH);
        if (!$finder->hasResults()) {
            return false;
        }
        $videos['videos'] = [];
        foreach($finder as $file){
            $videos['videos'][] = $this->findOne($file->getFilenameWithoutExtension());
        }
        return $videos;
    }

    public function insert($video)
    {
        $uuid = Uuid::uuid4();
        $video['id'] = $uuid;
        if(false !== file_put_contents($this->getFileName($uuid), json_encode($video))){
            return $video;
        }
        return false;
    }

    public function delete($uuid)
    {
        if(!file_exists($this->getFileName($uuid))){
            return false;
        }

        unlink($this->getFileName($uuid));
    }

    private function getFileName($uuid)
    {
        return sprintf('%s/%s.json', self::DATASTORE_PATH, $uuid);
    }

}