<?php 

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Filesystem\Filesystem;
use App\Service\VideoService;

class PostControllerTest extends WebTestCase
{
    private static $uuid;

    public function testCreate()
    {
        $client = static::createClient();
        $expected = [
            'name' => 'functional_test',
            'duration' => '30'
        ];
        $client->request(
            'POST', 
            '/videos',
            $expected
        );
    
        $this->assertEquals(201, $client->getResponse()->getStatusCode());
        $response = json_decode($client->getResponse()->getContent(), true);
        $expected['id'] = $response['id'];
        $this->assertEquals($expected, $response);
        self::$uuid = $response['id'];
    }

    public function testGetOne()
    {
        $client = static::createClient();
        $expected = [
            'name' => 'functional_test',
            'duration' => '30',
            'id' => self::$uuid
        ];
        $client->request(
            'GET', 
            '/videos/'.self::$uuid
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $response = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals($expected, $response);
    }

    public function testGetAll()
    {
        $client = static::createClient();
        $client->request('GET', '/videos');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testDelete()
    {
        $client = static::createClient();
        $expected = null;
        $client->request(
            'DELETE', 
            '/videos/'.self::$uuid
        );
        
        $this->assertEquals(204, $client->getResponse()->getStatusCode());
        $response = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals($expected, $response);
    }

}